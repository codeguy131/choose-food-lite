//!
//! Enter food choices as space seperated arguments
//! to the program.
//!
//! Will choose a food choice at random
//!
use rand::{thread_rng, Rng};
use std::{env::args, ops::RangeInclusive};

fn main() {
    let mut r = thread_rng();
    let r_index: usize = r.gen_range(RangeInclusive::new(1, args().len() - 1));
    println!("{}", args().nth(r_index).unwrap())
}
